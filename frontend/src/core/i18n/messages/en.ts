import { TranslationMessages } from "ra-core";
import englishMessages from "ra-language-english";
import { mergeMessages } from "./mergeMessages";

const messages: TranslationMessages = {
  ...englishMessages,

  resources: {
    Car: {
      name: "Car |||| Cars",

      fields: {
        description: "Description",
        name: "Name",
        type: "Type"
      }
    }
  },

  enums: {
    Type: {
      COUPE: "Coupe",
      HATCHBACK: "Hatchback",
      SEDAN: "Sedan"
    }
  }
};

export const en = mergeMessages(
  messages,
  [] // place addon messages here
);

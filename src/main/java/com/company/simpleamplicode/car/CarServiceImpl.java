package com.company.simpleamplicode.car;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CarServiceImpl implements CarService {
    public long count(Example<Car> example) {
        return carRepository.count(example);
    }

    private final CarRepository carRepository;

    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public long count() {
        return carRepository.count();
    }

    public void delete(Car entity) {
        carRepository.delete(entity);
    }

    public Car getById(UUID id) {
        return carRepository.getById(id);
    }

    public Car save(Car entity) {
        return carRepository.save(entity);
    }
}

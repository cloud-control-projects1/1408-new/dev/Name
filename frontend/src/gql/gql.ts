/* eslint-disable */
import * as types from "./graphql";
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
  "mutation UpdateCar($input: CarInput!) {\n  updateCar(input: $input) {\n    description\n    id\n    name\n    type\n  }\n}":
    types.UpdateCarDocument,
  "query Car($id: ID!) {\n  car(id: $id) {\n    description\n    id\n    name\n    type\n  }\n}":
    types.CarDocument,
  "query CarList {\n  carList {\n    description\n    id\n    name\n    type\n  }\n}":
    types.CarListDocument,
  "mutation DeleteCar($id: ID!) {\n  deleteCar(id: $id) \n}":
    types.DeleteCarDocument,
  "\n     query userPermissions {\n         userPermissions\n     }\n":
    types.UserPermissionsDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function gql(source: string): unknown;

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "mutation UpdateCar($input: CarInput!) {\n  updateCar(input: $input) {\n    description\n    id\n    name\n    type\n  }\n}"
): (typeof documents)["mutation UpdateCar($input: CarInput!) {\n  updateCar(input: $input) {\n    description\n    id\n    name\n    type\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "query Car($id: ID!) {\n  car(id: $id) {\n    description\n    id\n    name\n    type\n  }\n}"
): (typeof documents)["query Car($id: ID!) {\n  car(id: $id) {\n    description\n    id\n    name\n    type\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "query CarList {\n  carList {\n    description\n    id\n    name\n    type\n  }\n}"
): (typeof documents)["query CarList {\n  carList {\n    description\n    id\n    name\n    type\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "mutation DeleteCar($id: ID!) {\n  deleteCar(id: $id) \n}"
): (typeof documents)["mutation DeleteCar($id: ID!) {\n  deleteCar(id: $id) \n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "\n     query userPermissions {\n         userPermissions\n     }\n"
): (typeof documents)["\n     query userPermissions {\n         userPermissions\n     }\n"];

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> =
  TDocumentNode extends DocumentNode<infer TType, any> ? TType : never;

import { gql } from "@amplicode/gql";
import { Type } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { useCallback } from "react";
import { Create, SimpleForm, TextInput, useCreate, useNotify, useRedirect } from "react-admin";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { checkServerValidationErrors } from "../../../core/error/checkServerValidationError";
import { EnumInput } from "../../../core/inputs/EnumInput";

const UPDATE_CAR = gql(`mutation UpdateCar($input: CarInput!) {
  updateCar(input: $input) {
    description
    id
    name
    type
  }
}`);

export const CarCreate = () => {
  const redirect = useRedirect();
  const notify = useNotify();
  const [create] = useCreate();

  const save: SubmitHandler<FieldValues> = useCallback(
    async (data: FieldValues) => {
      try {
        const params = { data, meta: { mutation: UPDATE_CAR } };
        const options = { returnPromise: true };

        await create("Car", params, options);

        notify("ra.notification.created", { messageArgs: { smart_count: 1 } });
        redirect("list", "Car");
      } catch (response: any) {
        console.log("create failed with error", response);
        return checkServerValidationErrors(response, notify);
      }
    },
    [create, notify, redirect]
  );

  return (
    <Create<ItemType> redirect="list">
      <SimpleForm onSubmit={save}>
        <TextInput source="description" name="description" />
        <TextInput source="name" name="name" />
        <EnumInput name="type" source="type" enumTypeName="Type" enum={Type} />
      </SimpleForm>
    </Create>
  );
};

const CAR_TYPE = gql(`query Car($id: ID!) {
  car(id: $id) {
    description
    id
    name
    type
  }
}`);

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof CAR_TYPE>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = { id: string } & Exclude<QueryResultType["car"], undefined>;

import { DevSupport } from "@react-buddy/ide-toolbox";
import { AdminContext, AdminUI, Loading, Resource } from "react-admin";
import { useAuthProvider } from "../authProvider/useAuthProvider";
import { getCarRecordRepresentation } from "../core/record-representation/getCarRecordRepresentation";
import { dataProvider } from "../dataProvider/graphqlDataProvider";
import { ComponentPreviews, useInitial } from "../dev";
import { i18nProvider } from "../i18nProvider";
import { AdminLayout } from "./AdminLayout";
import { CarCreate } from "./screens/car/CarCreate";
import { CarEdit } from "./screens/car/CarEdit";
import { CarList } from "./screens/car/CarList";

export const App = () => {
  const { authProvider, loading } = useAuthProvider();

  if (loading) {
    return (
      <Loading
        loadingPrimary="Loading"
        loadingSecondary="The page is loading, just a moment please"
      />
    );
  }

  return (
    <AdminContext
      dataProvider={dataProvider}
      authProvider={authProvider}
      i18nProvider={i18nProvider}
    >
      <DevSupport ComponentPreviews={ComponentPreviews} useInitialHook={useInitial}>
        <AdminUI layout={AdminLayout}>
          <Resource
            name="Car"
            options={{ label: "Car" }}
            list={CarList}
            recordRepresentation={getCarRecordRepresentation}
            create={CarCreate}
            edit={CarEdit}
          />
        </AdminUI>
      </DevSupport>
    </AdminContext>
  );
};

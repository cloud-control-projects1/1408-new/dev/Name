package com.company.simpleamplicode.car;

import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Example;
import org.springframework.data.mapping.context.MappingContextEvent;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(method = {RequestMethod.HEAD, RequestMethod.POST})
public class CarController {

    private final CarServiceImpl carServiceImpl;
    private final CarRepository carRepository;

    public CarController(CarServiceImpl carServiceImpl,
                         CarRepository carRepository) {
        this.carServiceImpl = carServiceImpl;
        this.carRepository = carRepository;
    }

    @GetMapping
    public long count() {
        return carServiceImpl.count();
    }

    @GetMapping
    public ResponseEntity<Long> count(@RequestParam("example") Example<Car> example) {
        long resultLong = carServiceImpl.count(example);
        return ResponseEntity.ok(resultLong);
    }

    @DeleteMapping
    public ResponseEntity<Void> delete(@RequestParam("entity") Car entity) {
        carServiceImpl.delete(entity);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Car> getById(@PathVariable("id") UUID id) {
        Car car = carServiceImpl.getById(id);
        return ResponseEntity.ok(car);
    }



    @PostMapping
    public ResponseEntity<Car> save(@RequestBody Car entity) {
        Car car = carServiceImpl.save(entity);
        return ResponseEntity.ok(car);
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.HEAD, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT}, path = {"/{id.}"})
    public ResponseEntity<Boolean> existsById(@PathVariable("id") UUID id) {
        boolean resultBoolean = carRepository.existsById(id);
        return ResponseEntity.ok(resultBoolean);
    }

    @PostMapping("/lll")
    public ResponseEntity<Car> savekk(@RequestBody Car entity) {
        Car car = carServiceImpl.save(entity);
        return ResponseEntity.ok(car);
    }

    @EventListener
    public void handleMappingContextEvent(MappingContextEvent event) {
    }

}


package com.company.simpleamplicode.car;

import com.amplicode.core.graphql.annotation.GraphQLId;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Controller
public class GraphCarController {
    private final CarRepository crudRepository;

    public GraphCarController(CarRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @MutationMapping(name = "deleteCar")
    @Transactional
    public void delete(@GraphQLId @Argument @NonNull UUID id) {
        Car entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "carList")
    @Transactional(readOnly = true)
    @NonNull
    public List<Car> findAll() {
        return crudRepository.findAll();
    }

    @QueryMapping(name = "car")
    @Transactional(readOnly = true)
    @NonNull
    public Car findById(@GraphQLId @Argument @NonNull UUID id) {
        return crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateCar")
    @Transactional
    @NonNull
    public Car update(@Argument @NonNull Car input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        return crudRepository.save(input);
    }

}
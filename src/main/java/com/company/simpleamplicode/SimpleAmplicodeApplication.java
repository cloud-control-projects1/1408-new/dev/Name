package com.company.simpleamplicode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SimpleAmplicodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleAmplicodeApplication.class, args);
    }
}

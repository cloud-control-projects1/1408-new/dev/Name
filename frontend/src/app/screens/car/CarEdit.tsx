import { gql } from "@amplicode/gql";
import { Type } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { useCallback } from "react";
import { Edit, SimpleForm, TextInput, useNotify, useRedirect, useUpdate } from "react-admin";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { checkServerValidationErrors } from "../../../core/error/checkServerValidationError";
import { EnumInput } from "../../../core/inputs/EnumInput";

const CAR = gql(`query Car($id: ID!) {
  car(id: $id) {
    description
    id
    name
    type
  }
}`);
const UPDATE_CAR = gql(`mutation UpdateCar($input: CarInput!) {
  updateCar(input: $input) {
    description
    id
    name
    type
  }
}`);

export const CarEdit = () => {
  const queryOptions = {
    meta: {
      query: CAR,
      resultDataPath: null,
    },
  };

  const redirect = useRedirect();
  const notify = useNotify();
  const [update] = useUpdate();

  const save: SubmitHandler<FieldValues> = useCallback(
    async (data: FieldValues) => {
      try {
        const params = { data, meta: { mutation: UPDATE_CAR } };
        const options = { returnPromise: true };

        await update("Car", params, options);

        notify("ra.notification.updated", { messageArgs: { smart_count: 1 } });
        redirect("list", "Car");
      } catch (response: any) {
        console.log("update failed with error", response);
        return checkServerValidationErrors(response, notify);
      }
    },
    [update, notify, redirect]
  );

  return (
    <Edit<ItemType> mutationMode="pessimistic" queryOptions={queryOptions}>
      <SimpleForm onSubmit={save}>
        <TextInput source="description" name="description" />
        <TextInput source="name" name="name" />
        <EnumInput name="type" source="type" enumTypeName="Type" enum={Type} />
      </SimpleForm>
    </Edit>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof CAR>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = { id: string } & Exclude<QueryResultType["car"], undefined>;

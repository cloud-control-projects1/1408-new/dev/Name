package com.company.simpleamplicode.car;

public enum Type {
    SEDAN, HATCHBACK, COUPE
}

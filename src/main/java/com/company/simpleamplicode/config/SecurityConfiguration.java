package com.company.simpleamplicode.config;

import com.amplicode.core.auth.AuthenticationInfoProvider;
import com.amplicode.core.auth.UserDetailsAuthenticationInfoProvider;
import com.amplicode.core.security.Authorities;
import com.amplicode.core.security.UnauthorizedStatusAuthenticationEntryPoint;
import com.amplicode.core.security.formlogin.FormLoginAuthenticationFailureHandler;
import com.amplicode.core.security.formlogin.FormLoginAuthenticationSuccessHandler;
import com.amplicode.core.security.formlogin.FormLoginLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration {

    private final String adminUsername;
    private final String adminPassword;

    public SecurityConfiguration(@Value("${app.security.in-memory.admin.username}") String adminUsername,
                                 @Value("${app.security.in-memory.admin.password}") String adminPassword) {
        this.adminUsername = adminUsername;
        this.adminPassword = adminPassword;
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new FormLoginAuthenticationSuccessHandler();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new FormLoginAuthenticationFailureHandler();
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new FormLoginLogoutSuccessHandler();
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new UnauthorizedStatusAuthenticationEntryPoint();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        //Form Login
        //Exception handling
        //Authorize
        //CORS
        http.cors(withDefaults());
        //Authorize Requests
        http.authorizeRequests(authorizeRequests -> authorizeRequests
                .antMatchers("/graphql/**").permitAll()
                .antMatchers("/graphql").permitAll());
        //Session management
        http.sessionManagement(withDefaults());
        //Access denied handler
        http.exceptionHandling(exceptionHandling -> exceptionHandling
                .authenticationEntryPoint(authenticationEntryPoint()));
        //Form login
        http.formLogin(formLogin -> formLogin
                .successHandler(formLoginAuthenticationSuccessHandler())
                .failureHandler(formLoginAuthenticationFailureHandler()));
        //Logout
        http.logout(logout -> logout
                .logoutSuccessHandler(logoutSuccessHandler()));
        //Anonymous
        http.anonymous(withDefaults());
        //CSRF
        http.csrf(AbstractHttpConfigurer::disable);
        //Headers management
        http.headers(AbstractHttpConfigurer::disable);
        //JWT Token
        http.oauth2ResourceServer(oauth2ResourceServer -> oauth2ResourceServer
                .jwt(withDefaults()));
        return http.build();
    }

    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        UserDetails admin = User.builder()
                .username(adminUsername)
                .password(adminPassword)
                .authorities("ROLE_ADMIN", Authorities.FULL_ACCESS)
                .build();

        return new InMemoryUserDetailsManager(admin);
    }

    @Bean
    public AuthenticationInfoProvider authenticationInfoProvider() {
        return new UserDetailsAuthenticationInfoProvider();
    }

    @Bean
    public UserDetailsService inMemoryUserDetailsService() {
        // The builder will ensure the passwords are encoded before saving in memory
        User.UserBuilder users = User.withDefaultPasswordEncoder();
        InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();
        userDetailsManager.createUser(users.username("admin")
                .password("admin")
                .roles("ADMIN")
                .build());
        userDetailsManager.createUser(users.username("user")
                .password("user")
                .roles("USER")
                .build());
        return userDetailsManager;
    }

    @Bean
    FormLoginAuthenticationSuccessHandler formLoginAuthenticationSuccessHandler() {
        return new FormLoginAuthenticationSuccessHandler();
    }

    @Bean
    FormLoginAuthenticationFailureHandler formLoginAuthenticationFailureHandler() {
        return new FormLoginAuthenticationFailureHandler();
    }
}

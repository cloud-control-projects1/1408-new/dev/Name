import { gql } from "@amplicode/gql";
import { Type } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { Datagrid, DeleteButton, EditButton, List, TextField } from "react-admin";
import { EnumField } from "../../../core/fields/EnumField";

const CAR_LIST = gql(`query CarList {
  carList {
    description
    id
    name
    type
  }
}`);

const DELETE_CAR = gql(`mutation DeleteCar($id: ID!) {
  deleteCar(id: $id) 
}`);

export const CarList = () => {
  const queryOptions = {
    meta: {
      query: CAR_LIST,
      resultDataPath: "",
    },
  };

  return (
    <List<ItemType> queryOptions={queryOptions} exporter={false} pagination={false}>
      <Datagrid rowClick="show" bulkActionButtons={false}>
        <TextField source="id" sortable={false} />

        <TextField source="description" sortable={false} />
        <TextField source="name" sortable={false} />
        <EnumField source="type" enumTypeName="Type" enum={Type} sortable={false} />

        <EditButton />
        <DeleteButton
          mutationMode="pessimistic"
          mutationOptions={{ meta: { mutation: DELETE_CAR } }}
        />
      </Datagrid>
    </List>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof CAR_LIST>;
/**
 * Type of the items list
 */
type ItemListType = QueryResultType["carList"];
/**
 * Type of single item
 */
type ItemType = { id: string } & Exclude<Exclude<ItemListType, null | undefined>[0], undefined>;
